# #4. Reactive Forms

## 0. Before start prepare backend :>
In `user.service.ts` add array with users:
```
export class Credentials {
  constructor(
    public username: string,
    public password: string
  ) { }
}
```
```
private knownCredentials: Array<Credentials> = [
  new Credentials('admin', 'admin')
];
```
Modify login function:
```
public logIn(credentials: Credentials) {
  return this.credentialsExists(credentials)
    .do(() => {
      this.isLoggedIn = true;
      localStorage.setItem('token', this.token);
      this.permissionService.updatePermission.emit();
    });
}
```
```
private credentialsExists(loggingCredentials: Credentials): Observable<void> {
  return new Observable<void>((subscriber) => {
    for (const credentials of this.knownCredentials) {
      if (
        loggingCredentials.username === credentials.username &&
        loggingCredentials.password === credentials.password
      ) {
        subscriber.next();
        subscriber.complete();

        return;
      }
    }
    subscriber.error();
    subscriber.complete();
  });
}
```

## 1. Import ReactiveFormsModule
In `app.module.ts` import ReactiveFormsModule:
```
import { ReactiveFormsModule } from '@angular/forms';
```
Add `ReactiveFormsModule` to `@ngModule imports`

In `login.component.ts`:
```
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
```

## 2. Create form by FormBuilder
- Create field `userLoginForm: FormGroup;` that represents our form.
- Inject `private formBuilder: FormBuilder`
- Create form:
```
this.userLoginForm = this.formBuilder.group({
  username: '',
  password: ''
});
```

## 3. Create form view
```
<form
  novalidate
  (ngSubmit)="login()"
  [formGroup]="userLoginForm"
>
  <label>Username:</label>
  <input
    formControlName="username"
    type="email"
  >
  <label>Password:</label>
  <input
    formControlName="password"
  >
  <button
    type="submit"
    class="waves-effect waves-light btn blue large"
  >
    Login
  </button>
</form>
```
`ngSubmit` parameter assumes method to call when submitting form.
`formGroup` parameter assumes our form field.

In `login.component.ts` create method:
```
public login() {
  if (this.userLoginForm.invalid) {
    return;
  }

  this.userService.logIn(
    new User(
      this.userLoginForm.get('username').value,
      this.userLoginForm.get('password').value
    )
  )
  .subscribe(
    () => {
      this.router.navigate(['']);
    },
    () => {
      alert('Błędny login lub hasło');
    }
  );
}
```
## 4. Validation
- Modify our form:
```
this.userLoginForm = this.formBuilder.group({
  username: ['', Validators.required],
  password: ['', Validators.required]
});
```
- Modify our template:
Add `[class.invalid]="userLoginForm.controls.username.touched && userLoginForm.controls.username.invalid"` to username input.

- We can disable button when form is invalid:
```
[disabled]="userLoginForm.invalid"
```