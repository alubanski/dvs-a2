import { DvsA2Page } from './app.po';

describe('dvs-a2 App', function() {
  let page: DvsA2Page;

  beforeEach(() => {
    page = new DvsA2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
