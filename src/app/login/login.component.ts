import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../user.service';
import { PermissionService } from '../permission.service';
import { Credentials } from '../credentials.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public userLoginForm: FormGroup;

  constructor(
    private router: Router,
    private userService: UserService,
    private permissionService: PermissionService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.userLoginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  public login() {
    if (this.userLoginForm.invalid) {
      return;
    }

    this.userService.logIn(
      new Credentials(
        this.userLoginForm.get('username').value,
        this.userLoginForm.get('password').value
      )
    )
    .subscribe(
      () => {
        this.permissionService.permissionsChanged();
        this.router.navigate(['']);
      },
      () => {
        alert('Błędny login lub hasło');
      }
    );
  }
}
