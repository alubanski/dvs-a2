import { Component, OnInit } from '@angular/core';
import { ROLE } from './../permission.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public ROLE = ROLE;

  constructor() { }

  ngOnInit() {
  }

}
