import { Directive, Input, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { PermissionService, ROLE } from './permission.service';

@Directive({
  selector: '[roleClass]'
})
export class RoleClassDirective implements OnInit, OnDestroy {

  @Input('roleClass')
  public classes: { [className: string]: Array<ROLE> };
  private permissionsChangpermissionsChangeSubscription: Subscription = null;

  constructor(
    private el: ElementRef,
    private permissionService: PermissionService
  ) {
    this.permissionsChangpermissionsChangeSubscription = this.permissionService.onPermissionsChange.subscribe(() => {
      this.updateClasses();
    });
  }

  ngOnInit() {
    this.updateClasses();
  }

  ngOnDestroy() {
    this.permissionsChangpermissionsChangeSubscription.unsubscribe();
  }

  private updateClasses(): void {
    for (const className in this.classes) {
      if (!this.classes.hasOwnProperty(className)) {
        continue;
      }

      const roles: Array<ROLE> = this.classes[className];
      if (this.permissionService.hasPermission(roles)) {
        this.addClass(className);
        continue;
      }

      this.removeClass(className);
    }
  }

  private addClass(className: string): void {
    const classNamesArray: Array<string> = className.split(' ');

    for (const item of classNamesArray) {
      this.el.nativeElement.classList.add(item);
    }
  }

  private removeClass(className: string): void {
    const classNamesArray: Array<string> = className.split(' ');

    for (const item of classNamesArray) {
      this.el.nativeElement.classList.remove(item);
    }
  }
}
