import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PermissionService, ROLE } from './permission.service';
import { UserService } from './user.service';
import { IfRoleDirective } from './if-role.directive';
import { RoleClassDirective } from './role-class.directive';

const appRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [PermissionService],
    data: {
      only: [ROLE.USER],
      redirectTo: 'login',
    }
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [PermissionService],
    data: {
      only: [ROLE.USER],
      redirectTo: 'login',
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [PermissionService],
    data: {
      only: [ROLE.GUEST],
      redirectTo: 'dashboard',
    }
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ProfileComponent,
    LoginComponent,
    PageNotFoundComponent,
    SidebarComponent,
    IfRoleDirective,
    RoleClassDirective
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule
  ],
  providers: [
    PermissionService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(
    userService: UserService,
    permissionService: PermissionService
  ) {

    permissionService.addRole(
      ROLE.GUEST,
      () => { return !userService.isLoggedIn; }
    );

    permissionService.addRole(
      ROLE.USER,
      () => { return userService.isLoggedIn; }
    );

  }

}
