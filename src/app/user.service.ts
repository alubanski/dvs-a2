import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

import { PermissionService } from './permission.service';
import { Credentials } from './credentials.model';

@Injectable()
export class UserService {
  public isLoggedIn: boolean = false;
  private token: string = 'szaaaa... to token';
  private knownCredentials: Array<Credentials> = [
    new Credentials('admin', 'admin')
  ];

  constructor(
      private permissionService: PermissionService
  ) {
    this.isLoggedIn = localStorage.getItem('token') === this.token;
  }

  public logIn(credential: Credentials) {
    return this.credentialsExists(credential)
      .do(() => {
        this.isLoggedIn = true;
        localStorage.setItem('token', this.token);
        this.permissionService.permissionsChanged();
      });
  }

  public logOut() {
    this.isLoggedIn = false;
    localStorage.removeItem('token');
    this.permissionService.permissionsChanged();
  }

  private credentialsExists(loggingCredential: Credentials): Observable<void> {
    return new Observable<void>((subscriber) => {
      for (const credential of this.knownCredentials) {
        if (
          loggingCredential.username === credential.username &&
          loggingCredential.password === credential.password
        ) {
          subscriber.next();
          subscriber.complete();

          return;
        }
      }

      subscriber.error();
      subscriber.complete();
    });
  }
}
